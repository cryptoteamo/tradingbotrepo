import math
import matplotlib.pyplot as pyplot
import numpy
import plotter


def AppendPricePercentChangedColumn(
    priceData,
    priceDataFileName,
    priceIndex,
    numberOfDecimalPlacesToRound,
    isHistogramGenerated):
    percentChanges = []
    previousPrice = -1.0

    for i in range(len(priceData)):
        currentPrice = priceData[i][priceIndex]
        if previousPrice <= 0.0:
            percentChanges.append(0.0)
        else:
            percentChange = round(currentPrice / previousPrice - 1, numberOfDecimalPlacesToRound)
            percentChanges.append(min(percentChange, 1.0)) # values must range between -1 and +1
        previousPrice = currentPrice

    percentChangesColumn = numpy.array(percentChanges)

    if isHistogramGenerated:
        binSize = 0.0025
        plotter.GenerateHistogram(
            percentChanges,
            f"percent change (bin size = {binSize})",
            'frequency',
            f"{priceDataFileName} - Consecutive Daily Open Price Difference",
            True,
            numpy.arange(-0.28, 0.29, binSize),
            numpy.arange(-0.28, 0.29, 0.01),
            f"./PreprocessorPlots/{priceDataFileName}_PercentChanges_DailyOpen.png")
        
    augmentedPriceNdArray = numpy.column_stack((priceData, percentChangesColumn))
    return augmentedPriceNdArray


def AppendPricePercentChangedHighLowColumn(
    priceData,
    priceDataFileName,
    highPriceIndex,
    lowPriceIndex,
    numberOfDecimalPlacesToRound,
    isHistogramGenerated):
    percentChanges = []

    for i in range(0, len(priceData)):
        highPrice = priceData[i][highPriceIndex]
        lowPrice = priceData[i][lowPriceIndex]

        percentChange = ((highPrice / lowPrice) - 1.0)
        percentChanges.append(round(percentChange, numberOfDecimalPlacesToRound))

    if isHistogramGenerated:
        binSize = 0.0025
        plotter.GenerateHistogram(
            percentChanges,
            f"percent change (bin size = {binSize})",
            'frequency',
            f"{priceDataFileName} - Daily High/Low Percent Difference",
            True,
            numpy.arange(-0.01, 0.5, binSize),
            numpy.arange(-0.01, 0.5, 0.01),
            f"./PreprocessorPlots/{priceDataFileName}_PercentChanges_DailyHighToLow.png")

    percentChangesColumn = numpy.array(percentChanges)
    augmentedPriceNdArray = numpy.column_stack((priceData, percentChangesColumn))
    return augmentedPriceNdArray


def GenerateHistogramForPricePercentChangedHighLow(
    priceData,
    priceDataFileName,
    highPriceIndex,
    lowPriceIndex,
    timeStepSpan):
    percentChanges = []

    for priceDataIndex in range(0, len(priceData) - timeStepSpan):
        lowPrice = priceData[priceDataIndex][lowPriceIndex]
        highPrice = priceData[priceDataIndex][highPriceIndex]

        for i in range(priceDataIndex + 1, priceDataIndex + timeStepSpan + 1):
            lowPrice = numpy.minimum(lowPrice, priceData[i][lowPriceIndex])
            highPrice = numpy.maximum(highPrice, priceData[i][highPriceIndex])
        
        percentChange = ((highPrice / lowPrice) - 1.0)
        percentChanges.append(percentChange)

    binSize = 0.0025
    plotter.GenerateHistogram(
        percentChanges,
        f"percent change (bin size = {binSize})",
        'frequency',
        f"{priceDataFileName} - High/Low Percent Difference Over {timeStepSpan} Consecutive Steps",
        True,
        numpy.arange(-0.01, 0.5, binSize),
        numpy.arange(-0.01, 0.5, 0.01),
        f"./PreprocessorPlots/{priceDataFileName}_PercentChanges_DailyHighToLow{timeStepSpan}Steps.png")


def PlotStepsToReachPositivePercentGains(
    priceData,
    priceDataFileName,
    openPriceIndex,
    highPriceIndex,
    maxPercentGainToConsider,
    maxStepsToRealizeMaxPercentGain,
    isPlotSaved):
    numerOfDataPoints = len(priceData)
    maxStartingPointsToAnalyze = numerOfDataPoints - maxStepsToRealizeMaxPercentGain
    
    percentGainsForAllStartingPoints = [] # 2D array of all computed data

    for startingPointIndex in range(0, numerOfDataPoints):
        # terminate for-loop when the max number of points to analyze has been reached
        if startingPointIndex > maxStartingPointsToAnalyze:
            break

        stepsToReachPercentGain = []  # element value = number of steps to meet or exceed the floor(percent gain) that equals the element index + 1.
        startingPointPrice = priceData[startingPointIndex][openPriceIndex]
    
        for futurePointIndex in range(startingPointIndex, numerOfDataPoints):
            # terminate for-loop if number of future points look at has exceeded max count
            if (futurePointIndex - startingPointIndex) > maxStepsToRealizeMaxPercentGain:
                stepsToReachPercentGain = _getFixedLengthArray(stepsToReachPercentGain, maxPercentGainToConsider)
                percentGainsForAllStartingPoints.append(stepsToReachPercentGain)
                break

            percentChangeTarget = len(stepsToReachPercentGain) + 1

            # terminate for-loop if the target max percent gain is hit
            if percentChangeTarget >= maxPercentGainToConsider:
                stepsToReachPercentGain = _getFixedLengthArray(stepsToReachPercentGain, maxPercentGainToConsider)
                percentGainsForAllStartingPoints.append(stepsToReachPercentGain)
                break

            maxPriceInCurrentStep = priceData[futurePointIndex][highPriceIndex]
            percentChange = (maxPriceInCurrentStep / startingPointPrice - 1) * 100.0

            if percentChange >= percentChangeTarget:
                numberStepsSinceStartingPoint = futurePointIndex - startingPointIndex
                numberOfItemsInArrayToSet = math.floor(percentChange - percentChangeTarget) + 1

                for i in range(0, numberOfItemsInArrayToSet):
                    stepsToReachPercentGain.append(numberStepsSinceStartingPoint)

    averageStepsToReachPercentGains = []

    for percentGainIndex in range(0, maxPercentGainToConsider):
        sum = 0
        numberOfPointsInSum = 0
        itemsForSamePercentGain = [i[percentGainIndex] for i in percentGainsForAllStartingPoints]

        for stepCount in itemsForSamePercentGain:
            if stepCount > -1:
                sum = sum + stepCount
                numberOfPointsInSum = numberOfPointsInSum + 1

        averageStepsForThisPercentGain = -1

        if  numberOfPointsInSum > 0:
            averageStepsForThisPercentGain = sum / numberOfPointsInSum

        averageStepsToReachPercentGains.append(averageStepsForThisPercentGain)
    
    percentGainIntegersList = [i for i in range(1, maxPercentGainToConsider + 1)]

    # generate plot
    figure, axes = pyplot.subplots()
    axes.plot(averageStepsToReachPercentGains, percentGainIntegersList)
    axes.grid(True)
    axes.set_title(f"average steps to reach percent gain, starting points: {maxStartingPointsToAnalyze}, max future steps considered: {maxStepsToRealizeMaxPercentGain}")
    axes.set_xlabel("time steps")
    axes.set_ylabel("percent gain")
    axes.set_xticks(averageStepsToReachPercentGains)
    axes.set_yticks(percentGainIntegersList)

    figManager = pyplot.get_current_fig_manager()
    figManager.window.showMaximized()
    pyplot.show(block=True)


def _getFixedLengthArray(arrayToUpdate, expectedLength):
    numberOfMissingElements = expectedLength - len(arrayToUpdate)
    for i in range(0, numberOfMissingElements):
        arrayToUpdate.append(-1)

    numberOfExtraElements = len(arrayToUpdate) - expectedLength
    for i in range(0, numberOfExtraElements):
        arrayToUpdate.pop()
    
    return arrayToUpdate

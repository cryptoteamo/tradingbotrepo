def GenerateDebugInfoTextFile(
    startDateTime,
    endDateTime,
    initialBalanceDollars,
    percentOfDollarsToAllocateToCoin,
    trades,
    numberOfDataPointsProcessed,
    balanceDollars,
    balanceCoin,
    firstProcessedDataPoint,
    lastProcessedDataPoint,
    priceDataFileName
    ):
    startOfRunLines = _getStartOfRunDebugLines(
        startDateTime,
        endDateTime,
        initialBalanceDollars,
        percentOfDollarsToAllocateToCoin)

    endOfRunLines = _getEndOfRunDebugLines(
        trades,
        numberOfDataPointsProcessed,
        balanceDollars,
        balanceCoin,
        firstProcessedDataPoint,
        lastProcessedDataPoint)

    debugLines = startOfRunLines + endOfRunLines
    
    startString = startDateTime.strftime("%Y-%m-%dT%H-%M")
    endString = endDateTime.strftime("%Y-%m-%dT%H-%M")
    debugFileName = "./TesterLogs/" + priceDataFileName + "_trades_from_" + startString + "_to_" + endString + ".txt"

    _writeLinesToTextFile(debugFileName, debugLines)


def _getStartOfRunDebugLines(
    startDateTime,
    endDateTime,
    initialBalanceDollars,
    percentOfDollarsToAllocateToCoin
    ):
    lines = []
    lines.append("----- START OF RUN PARAMETERS -----")
    lines.append("start datetime: " + startDateTime.strftime("%Y-%m-%d %H:%M:%S"))
    lines.append("end datetime: " + endDateTime.strftime("%Y-%m-%d %H:%M:%S"))
    lines.append("initial dollars balance: " +  "{:.2f}".format(initialBalanceDollars))
    lines.append("percent of dollars to initially allocate to coin (0.0 to 100.0): " + "{:.2f}".format(percentOfDollarsToAllocateToCoin))

    return lines


def _getEndOfRunDebugLines(
    trades,
    numberOfDataPointsProcessed,
    finalDollars,
    finalCoin,
    firstProcessedDataPoint,
    lastProcessedDataPoint
    ):
    lines = []

    lines.append(" ")
    lines.append("----- END OF RUN RESULTS -----")
    lines.append("number of data points processed: " + "{:.1f}".format(numberOfDataPointsProcessed))
    lines.append("number of trades made: " + "{:.1f}".format(len(trades)))
    lines.append("final dollars balance: " + "{:.2f}".format(finalDollars))
    lines.append("final coin balance: " + "{:.10f}".format(finalCoin))

    finalNetBalanceInDollars = lastProcessedDataPoint[2] + lastProcessedDataPoint[3] * lastProcessedDataPoint[1]
    lines.append("final net portfolio value in dollars: " + "{:.2f}".format(finalNetBalanceInDollars))

    initialNetBalanceInDollars = firstProcessedDataPoint[2] + firstProcessedDataPoint[3] * firstProcessedDataPoint[1]
    lines.append("intial net portfolio value in dollars: " + "{:.2f}".format(initialNetBalanceInDollars))

    profitFromTrading = finalNetBalanceInDollars - initialNetBalanceInDollars
    lines.append("net profit with trading in dollars: " + "{:.2f}".format(profitFromTrading))

    # Compare the trading scheme to a simple HODL strategy
    maxInitialCoin = firstProcessedDataPoint[3] + firstProcessedDataPoint[2] / firstProcessedDataPoint[1]
    finalValueOfBuyAndHoldInDollars = maxInitialCoin * lastProcessedDataPoint[1]
    profitFromBuyAndHold = finalValueOfBuyAndHoldInDollars - initialNetBalanceInDollars
    
    lines.append(" ")
    lines.append("----- HODL strategy results (buy max coins on first processed data point, sell all coins on last processed data point) ----- ")
    lines.append("final net portfolio value in dollars for HODL strategy: " + "{:.2f}".format(finalValueOfBuyAndHoldInDollars))
    lines.append("net profit with a HODL strategy in dollars: " + "{:.2f}".format(profitFromBuyAndHold))
    lines.append(" ")

    return lines


def _writeLinesToTextFile(textFileName, lines):
    with open(textFileName, "w") as fileToWrite:
        for line in lines:
            fileToWrite.write(line)
            fileToWrite.write('\n')
        fileToWrite.close()
    
    print("Wrote debugging lines to text file: ", textFileName)

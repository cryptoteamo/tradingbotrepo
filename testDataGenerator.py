import datetime
import matplotlib.pyplot as pyplot
import math
import numpy
import plotter


def GetSineWavePriceData(
    numberOfStepsInCycle,
    numberOfCycles,
    decimalPrecision,
    amplitude,
    startDateTime,
    isPlotGenerated = False,
    dateStringFormat = "%Y-%m-%d",
    ):
    currentDateTime = startDateTime
    thetas = numpy.linspace(0, 2*(math.pi), numberOfStepsInCycle, endpoint=False)

    priceData = [] # should match the format of BitcoinData.csv
    dates = [] # keep track of this separately to make plotting easier
    prices = [] # keep track of this separately to make plotting easier

    for n in range(0, numberOfCycles):
        for i in range (0, numberOfStepsInCycle):
            price = round(amplitude*math.sin(thetas[i]) + amplitude*2, decimalPrecision)
            prices.append(price)

            date = currentDateTime.strftime(dateStringFormat)
            dates.append(date)

            priceData.append(['BTC', date, price, price, price, price])

            currentDateTime += datetime.timedelta(days=1)

    if isPlotGenerated == True:
        plotter.GeneratePlot(
            dates,
            prices,
            'date',
            'price',
            'test sine wave data!')

    return priceData

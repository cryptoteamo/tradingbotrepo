import datetime
import os
import numpy
import pandas
import plotter
import tester

# returns an object of type numpy.ndarray
def GetCsvFileRows(csvFileName):
    priceDataFilePath = "CoinPriceData/" + csvFileName + ".csv"
    csvFile = pandas.read_csv(priceDataFilePath)
    # csvFileColumns = csvFile.columns
    csvFileRows = csvFile.values # this does not include headers
    return csvFileRows


def ExecuteTradesOverDateRange(
    csvFileRows,
    priceDataFileName,
    priceDataCoinNameIndex,
    priceDataDateIndex,
    priceDataPriceIndex,
    startDateTime,
    endDateTime,
    initialDollars,
    percentOfDollarsToAllocateToCoin,
    isDebugTextFileWritten,
    isTradesCsvFileWritten,
    isPlotGenerated
    ):
    # run the tester and get the trades
    trades = tester.TestBotOverDateRange(
        csvFileRows,
        priceDataCoinNameIndex,
        priceDataDateIndex,
        priceDataPriceIndex,
        startDateTime,
        endDateTime,
        initialDollars,
        percentOfDollarsToAllocateToCoin,
        isDebugTextFileWritten,
        priceDataFileName)

    if isTradesCsvFileWritten == True:
        # write the trades to a CSV file
        start = startDateTime.strftime("%Y-%m-%dT%H-%M")
        end = endDateTime.strftime("%Y-%m-%dT%H-%M")

        tradesCsvFileName = "./TestRunnerCsvFiles/" + priceDataFileName + "_trades_from_" + start + "_to_" + end + ".csv"
        _writeTradesToCsv(trades, tradesCsvFileName)

    if isPlotGenerated == True:
        # plot the price data with green/red markers indicating the buy/sell locations
        priceDataWithinDateRange = _getRowsWithinDateRange(
            csvFileRows,
            priceDataDateIndex,
            startDateTime,
            endDateTime)

        tradeDataTradeTypeIndex = 0
        tradeDataDateIndex = 3
        tradeDataPriceIndex = 4
        tradeDataCoinNameIndex = 12

        start = startDateTime.strftime("%Y-%m-%dT%H-%M")
        end = endDateTime.strftime("%Y-%m-%dT%H-%M")

        tradesPlotFileName = "TestRunnerPlots/" + priceDataFileName + "_trades_from_" + start + "_to_" + end + ".png"

        _plotTrades(
            trades,
            tradeDataCoinNameIndex,
            tradeDataDateIndex,
            tradeDataPriceIndex,
            tradeDataTradeTypeIndex,
            priceDataWithinDateRange,
            priceDataDateIndex,
            priceDataPriceIndex,
            tradesPlotFileName)


def _writeTradesToCsv(trades, fileName):
    data = []
    for trade in trades:
        data.append({
            "Trade Type": trade[0],
            "Buy Confidence": trade[1],
            "Sell Confidence": trade[2],
            "Trade Timestamp": trade[3],
            "Coin Price (Dollars)": trade[4],
            "Balance Before Trade (Dollars)": trade[5],
            "Balance After Trade (Dollars)": trade[6],
            "Coin Balance Before Trade": trade[7],
            "Coin Balance After Trade": trade[8],
            "Net Portfolio Value (Dollars)": trade[9],
            "Trading Fee": trade[10],
            "Trading Fee Cost (Dollars)": trade[11],
            "Coin Name": trade[12]
        })

    dataFrame = pandas.DataFrame(data)

    _removeFileIfPresent(fileName)

    dataFrame.to_csv(fileName, index=False)
    print("Wrote trades to CSV file: ", fileName)


def _getRowsWithinDateRange(
    csvFileRows,
    dateIndex,
    startDateTime,
    endDateTime):
    rowsWithinDateRange = []

    for i in range(len(csvFileRows)):
        dateString = csvFileRows[i][dateIndex]
        date = datetime.datetime.strptime(dateString, "%Y-%m-%d")

        # only process data between the specified dates
        if (date >= startDateTime and date <= endDateTime):
            rowsWithinDateRange.append(csvFileRows[i])
    
    return rowsWithinDateRange


def _removeFileIfPresent(filePath):
    if os.path.exists(filePath):
        os.remove(filePath)
        print("Removed existing file: ", filePath)


def _plotTrades(
    trades,
    tradeCoinNameIndex,
    tradeDateIndex,
    tradePriceIndex,
    tradeTypeIndex,
    priceData,
    priceDataDateIndex,
    priceDataPriceIndex,
    fileName
    ):
    coinName = trades[0][tradeCoinNameIndex]
    xAxisLabel = "Datetime"
    yAxisLabel = "Price of " + coinName + " (Dollars)"
    title = coinName + " Trades"
    isFigureSaved = True

    dates = [i[priceDataDateIndex] for i in priceData]
    prices = [i[priceDataPriceIndex] for i in priceData]

    buyDates = []
    buyPrices = []
    sellDates = []
    sellPrices = []

    for trade in trades:
        tradePrice = trade[tradePriceIndex]
        tradeDate = trade[tradeDateIndex]
        tradeType = trade[tradeTypeIndex]

        if tradeType == "buy":
            buyDates.append(tradeDate)
            buyPrices.append(tradePrice)
        
        if tradeType == "sell":
            sellDates.append(tradeDate)
            sellPrices.append(tradePrice)

    plotter.GenerateLinePlotWithGreenAndRedMarkers(
        dates,
        prices,
        buyDates,
        buyPrices,
        sellDates,
        sellPrices,
        xAxisLabel,
        yAxisLabel,
        title,
        fileName,
        isFigureSaved)

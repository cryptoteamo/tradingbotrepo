import datetime
import testDataGenerator
import testRunnerHelper as helper
import tradingDataPreprocessor as preprocessor


# specify the data that gets written from the test run(s)
isDebugTextFileWritten = True
isTradesCsvFileWritten = False
isTradesPlotGenerated = True
isPreprocessorHistogramGenerated = True

# convert CSV file data to a numpy.ndarray
priceDataFileName = "BitcoinSineWaveData"

# specify the zero-indexed column number in csv file data for
# the coin name, open price, high price, low price, and date
priceDataCoinNameIndex = 0
priceDataDateIndex = 1
priceDataOpenIndex = 3

# specify the date range to trade over
startDateTime = datetime.datetime(2018, 10, 1, 0, 0, 0, 0)
endDateTime = datetime.datetime(2020, 10, 1, 0, 0, 0, 0)

# use dummy data!
dummyData = testDataGenerator.GetSineWavePriceData(
    32,
    24,
    5,
    0.25,
    startDateTime,
    True,
    "%Y-%m-%d")

# round to nearest tenth of a percent, which yields 2000 discrete values (probably too many bins for daily price data!)
percentChangePrecision = 3

# do some preprocessing of the price data
dummyData = preprocessor.AppendPricePercentChangedColumn(
    dummyData,
    priceDataFileName,
    priceDataOpenIndex,
    percentChangePrecision,
    isPreprocessorHistogramGenerated)

# initially have half of net worth in dollars and half in coin
initialDollars = 1000.0
percentOfDollarsToAllocateToCoin = 50.0

# run the test!
helper.ExecuteTradesOverDateRange(
    dummyData,
    priceDataFileName,
    priceDataCoinNameIndex,
    priceDataDateIndex,
    priceDataOpenIndex,
    startDateTime,
    endDateTime,
    initialDollars,
    percentOfDollarsToAllocateToCoin,
    isDebugTextFileWritten,
    isTradesCsvFileWritten,
    isTradesPlotGenerated)

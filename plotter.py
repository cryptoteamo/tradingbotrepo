import math
import matplotlib.pyplot as pyplot
import numpy
import os


def GenerateLinePlotWithGreenAndRedMarkers(
    xValues,
    yValues,
    greenMarkerXValues,
    greenMarkerYValues,
    redMarkerXValues,
    redMarkerYValues,
    xAxisLabel,
    yAxisLabel,
    plotTitle,
    figureFileName,
    isFigureSaved,
    isVerticalAxisLogScale = True):
    figure, axes = pyplot.subplots()

    axes.plot(xValues, yValues)
    axes.scatter(greenMarkerXValues, greenMarkerYValues, marker="P", s=80, color="#00FF00")
    axes.scatter(redMarkerXValues, redMarkerYValues, marker="v", s=80, color="#FF0000")

    axes.grid(False) # turning on grid lines (passing in True here) can significantly decrease performance
    axes.set_title(plotTitle)
    axes.set_xlabel(xAxisLabel)
    axes.set_ylabel(yAxisLabel)

    if isVerticalAxisLogScale == True:
        axes.set_yscale('log')

    # limit the number of visible labels on the x-axis
    numberOfXTicks = len(axes.xaxis.get_ticklabels())
    if (numberOfXTicks > 50):
        scaleFactor = numpy.floor(numberOfXTicks / 50)
        for i, tick in enumerate(axes.xaxis.get_ticklabels()):
            if i % scaleFactor != 0:
                tick.set_visible(False)
    
    # rotate the x-axis labels so they don't overlap with each other
    pyplot.xticks(rotation=90) 

    if isFigureSaved:
        figure.set_size_inches(28, 16)
        pyplot.savefig(figureFileName, dpi=200, bbox_inches='tight')
        print("Saved plot to file: ", figureFileName)

    # initially show the plot in a maximized window
    figManager = pyplot.get_current_fig_manager()
    figManager.window.showMaximized()
    
    # set block=True to pause the program until you close the external plot window
    pyplot.show(block=True)


def GeneratePlot(
    xValues,
    yValues,
    xAxisLabel,
    yAxisLabel,
    plotTitle,
    maxTicksXAxis = 50,
    showGridLines = False):
    figure, axes = pyplot.subplots()
    axes.plot(xValues, yValues)
    axes.grid(showGridLines)
    axes.set_title(plotTitle)
    axes.set_xlabel(xAxisLabel)
    axes.set_ylabel(yAxisLabel)

    # limit the number of visible labels on the x-axis
    numberOfXTicks = len(axes.xaxis.get_ticklabels())
    if (numberOfXTicks > maxTicksXAxis):
        scaleFactor = numpy.floor(numberOfXTicks / maxTicksXAxis)
        for i, tick in enumerate(axes.xaxis.get_ticklabels()):
            if i % scaleFactor != 0:
                tick.set_visible(False)
    
    # rotate the x-axis labels so they don't overlap with each other
    pyplot.xticks(rotation=90) 

    figManager = pyplot.get_current_fig_manager()
    figManager.window.showMaximized()
    pyplot.show(block=True)


def GenerateHistogram(
    dataArray,
    xAxisLabel,
    yAxisLabel,
    plotTitle,
    isFigureSaved,
    bins = [],
    xticks = [],
    figureFileName = 'figure'
    ):
    figure, axes = pyplot.subplots()

    if (len(bins) > 0):
        pyplot.hist(x=dataArray, bins=bins)
    else:
        pyplot.hist(x=dataArray, bins='auto')

    if (len(xticks) > 0):
        pyplot.xticks(xticks)

    npData = numpy.array(dataArray)

    dataMean = round(npData.mean(), 4)
    dataMedian = round(numpy.median(npData), 4) # this works even if npData is not sorted
    dataMin = round(npData.min(), 4)
    dataMax = round(npData.max(), 4)

    pyplot.axvline(dataMean, color='#00FF00', linestyle='dashed', linewidth=2)
    pyplot.axvline(dataMedian, color='#FFBB00', linestyle='dashed', linewidth=2)

    plotText = f"mean: {dataMean}\nmedian: {dataMedian}\nmin: {dataMin}\nmax: {dataMax}\ntotal count: {len(dataArray)}"
    props = dict(boxstyle='round', facecolor='#EEEEEE')
    axes.text(0.85, 0.95, plotText, fontsize=14, transform=axes.transAxes, verticalalignment='top', bbox=props)

    pyplot.grid(axis='both')
    pyplot.xlabel(xAxisLabel)
    pyplot.ylabel(yAxisLabel)
    pyplot.title(plotTitle)

    figure = pyplot.gcf()
    figure.set_size_inches(28, 16)

    if isFigureSaved:
        pyplot.savefig(figureFileName, dpi=200, bbox_inches='tight')
        print("Saved plot to file: ", figureFileName)

    # initially show the plot in a maximized window
    figManager = pyplot.get_current_fig_manager()
    figManager.window.showMaximized()

    # set block=True to pause the program until you close the external plot window
    pyplot.show(block=True)

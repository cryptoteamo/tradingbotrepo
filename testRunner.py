import datetime
import testRunnerHelper as helper
import tradingDataPreprocessor as preprocessor


# specify the data that gets written from the test run(s)
isDebugTextFileWritten = False
isTradesCsvFileWritten = False
isTradesPlotGenerated = True
isPreprocessorHistogramGenerated = False

# convert CSV file data to a numpy.ndarray
priceDataFileName = "BitcoinDailyData"
csvFileRows = helper.GetCsvFileRows(priceDataFileName)

# specify the zero-indexed column number in csv file data for
# the coin name, open price, high price, low price, and date
priceDataCoinNameIndex = 0
priceDataDateIndex = 1
priceDataOpenIndex = 3
priceDataHighIndex = 4
priceDataLowIndex = 5

# round to nearest tenth of a percent, which yields 2000 discrete values (probably too many bins for daily price data!)
percentChangePrecision = 3

# do some preprocessing of the price data
csvFileRows = preprocessor.AppendPricePercentChangedColumn(
    csvFileRows,
    priceDataFileName,
    priceDataOpenIndex,
    percentChangePrecision,
    isPreprocessorHistogramGenerated)

csvFileRows = preprocessor.AppendPricePercentChangedHighLowColumn(
    csvFileRows,
    priceDataFileName,
    priceDataHighIndex,
    priceDataLowIndex,
    percentChangePrecision,
    isPreprocessorHistogramGenerated)

# initially have half of net worth in dollars and half in coin
initialDollars = 1000.0
percentOfDollarsToAllocateToCoin = 50.0

# TODO: loop over start/end dates (shifting by 1 month)
# specify the date range to trade over
startDateTime = datetime.datetime(2018, 10, 1, 0, 0, 0, 0)
endDateTime = datetime.datetime(2020, 10, 1, 0, 0, 0, 0)

# run the test!
helper.ExecuteTradesOverDateRange(
    csvFileRows,
    priceDataFileName,
    priceDataCoinNameIndex,
    priceDataDateIndex,
    priceDataOpenIndex,
    startDateTime,
    endDateTime,
    initialDollars,
    percentOfDollarsToAllocateToCoin,
    isDebugTextFileWritten,
    isTradesCsvFileWritten,
    isTradesPlotGenerated)

import datetime
import testerHelper


def TestBotOverDateRange(
    priceData,
    coinNameIndex,
    dateIndex,
    priceIndex,
    startDateTime,
    endDateTime,
    initialBalanceDollars,
    percentOfDollarsToAllocateToCoin,
    isDebugTextFileWritten,
    priceDataFileName
    ):
    balanceDollars = initialBalanceDollars
    firstProcessedDataPoint = [startDateTime, 0.0, balanceDollars, 0.0]
    lastProcessedDataPoint = [endDateTime, 0.0, balanceDollars, 0.0]
    balanceCoin = 0.0
    numberOfDataPointsProcessed = 0
    trades = []

    # loop over CSV data
    for i in range(len(priceData)):
        dateString = priceData[i][dateIndex]
        date = datetime.datetime.strptime(dateString, "%Y-%m-%d")

        # only process data between the specified dates
        if (date >= startDateTime and date <= endDateTime):
            coinName = priceData[i][coinNameIndex]
            openPrice = priceData[i][priceIndex]

            numberOfDataPointsProcessed = numberOfDataPointsProcessed + 1

            if (numberOfDataPointsProcessed == 1):
                balanceDollars = balanceDollars / 2.0
                balanceCoin = balanceDollars / openPrice
                firstProcessedDataPoint = [
                    date.strftime("%Y-%m-%d %H:%M:%S"),
                    openPrice,
                    balanceDollars,
                    balanceCoin]

            confidenceToBuy = 0.5
            confidenceToSell = 0.5

            # TODO: use a better criteria for buying/selling
            if (date.day == 1):
                confidenceToBuy = 1.0
                confidenceToSell = 0.0
            if (date.day == 15):
                confidenceToSell = 1.0
                confidenceToBuy = 0.0

            trade = _executeTrade(
                date,
                openPrice,
                balanceDollars,
                balanceCoin,
                confidenceToBuy,
                confidenceToSell)

            if (len(trade) > 0):
                trade.append(coinName)
                trades.append(trade)
                balanceDollars = trade[6]
                balanceCoin = trade[8]

            lastProcessedDataPoint = [
                date.strftime("%Y-%m-%d %H:%M:%S"),
                openPrice,
                balanceDollars,
                balanceCoin]
    # END loop over CSV data

    if (isDebugTextFileWritten == True):
        testerHelper.GenerateDebugInfoTextFile(
            startDateTime,
            endDateTime,
            initialBalanceDollars,
            percentOfDollarsToAllocateToCoin,
            trades,
            numberOfDataPointsProcessed,
            balanceDollars,
            balanceCoin,
            firstProcessedDataPoint,
            lastProcessedDataPoint,
            priceDataFileName)

    return trades


def _executeTrade(
    transactionDateTime,
    priceOfCoinInDollars,
    balanceDollars,
    balanceCoin,
    confidenceToBuy,
    confidenceToSell
    ):
    # TODO: get buy/sell confidence thresholds from somewhere
    confidenceThreshold = 0.7

    if (confidenceToBuy < confidenceThreshold and confidenceToSell < confidenceThreshold):
        return []
    
    finalBalanceDollars = balanceDollars
    finalBalanceCoin = balanceCoin
    tradeType = "none"

    tradingFee = 0.005 # this is the Coinbase maker/taker fee in pricing tier < $10K
    tradingFeeCost = 0.0

    if (confidenceToBuy >= confidenceThreshold):
        tradeType = "buy"
        tradingFeeCost = balanceDollars * tradingFee
        coinToBuy = (balanceDollars - tradingFeeCost) / priceOfCoinInDollars
        finalBalanceCoin = balanceCoin + coinToBuy
        finalBalanceDollars = 0.0
    if (confidenceToSell >= confidenceThreshold):
        tradeType = "sell"
        dollarValueOfSoldCoin = balanceCoin * priceOfCoinInDollars
        tradingFeeCost = dollarValueOfSoldCoin * tradingFee
        finalBalanceDollars = balanceDollars + dollarValueOfSoldCoin - tradingFeeCost
        finalBalanceCoin = 0.0

    dateString = transactionDateTime.strftime("%Y-%m-%d") # %H:%M:%S
    netPortfolioValue = finalBalanceCoin * priceOfCoinInDollars + finalBalanceDollars

    trade = [
        tradeType,
        confidenceToBuy,
        confidenceToSell,
        dateString,
        priceOfCoinInDollars,
        balanceDollars,
        finalBalanceDollars,
        balanceCoin,
        finalBalanceCoin,
        netPortfolioValue,
        tradingFee,
        tradingFeeCost]

    return trade
